
$(document).ready(function () {
    $('#cadastro').on('submit', function (e) {
        e.preventDefault(); // evita que o formulário 
        var formData = new FormData(this);
        $.ajax({
            type: 'POST',
            url: '/login/cadastrar',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,

            success: function (re) {
                $('#retornoCad').html(re);
            },
            beforeSend: function () {
                $("#processando").css({display: "block"});
            },
            complete: function () {
                $("#processando").css({display: "none"});
            },
            error: function () {
                $("#retornoCad").html("Erro em chamar a função.");
                setTimeout(function () {
                    $("#retornoCad").css({display: "none"});
                }, 5000);
            }
        });
    });
});





$(document).ready(function () {
    $('#login').on('submit', function (e) {
        e.preventDefault(); // evita que o formulário 
        var formData = new FormData(this);
        $.ajax({
            type: 'POST',
            url: '/login/validar',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,

            success: function (re) {
                $('#retorno').html(re);
            },
            beforeSend: function () {
                $("#processando").css({display: "block"});
            },
            complete: function () {
                $("#processando").css({display: "none"});
            },
            error: function () {
                $("#div_retorno").html("Erro em chamar a função.");
                setTimeout(function () {
                    $("#div_retorno").css({display: "none"});
                }, 5000);
            }
        });
    });
});

$("#codigo").blur(function () {
    if ($('#acao').val() == 'cadastrar') {
        var cod = $('#codigo').val();
        $.ajax({
            type: 'POST',
            url: '/verificarCodigo',
            data: {'cod': cod},

            success: function (re) {
                $('#retorno-cod').html(re);
            },
            error: function () {
                $("#retorno").html("Erro em chamar a função.");
                setTimeout(function () {
                    $("#retorno").css({display: "none"});
                }, 5000);
            }
        });
    }
    return;

});
$(document).ready(function () {
    $('#salvar-peca').on('submit', function (e) {
        e.preventDefault(); // evita que o formulário 
        var formData = new FormData(this);
        var acao = document.getElementById('acao').value;
        var url;
        if (confirm("Confirma os Dados?")) {
            if (acao == 'cadastrar') {
                url = '/admin/cadastrar-pecas';
            } else {
                url = '/admin/alterar-peca';
            }
            $.ajax({
                type: 'POST',
                url: url,
                data: formData,
                cache: false,
                contentType: false,
                processData: false,

                success: function (re) {
                    $('#retorno-1').html(re);
                },
                error: function () {
                    $("#retorno").html("Erro em chamar a função.");
                    setTimeout(function () {
                        $("#retorno").css({display: "none"});
                    }, 5000);
                }
            });
        }

    });
});


$(document).ready(function () {


    $("*[name='tipo']").change(function () {
        tipo = ($(this).attr('value'));
        if (tipo == 'fisica') {
            $('#nome').attr({placeholder: 'Nome'});
            $('#sobrenome').attr({placeholder: 'Sobrenome'});
            $('#documento').attr({placeholder: 'CPF'});

        } else {
            $('#nome').attr({placeholder: 'Como quer ser chamado'});
            $('#sobrenome').attr({placeholder: 'Nome da Compania'});
            $('#documento').attr({placeholder: 'CNPJ'});

        }
    });



});

$(document).ready(function () {
    $('#documento').on('click', function (e) {
        tipo = $("input[name='tipo']:checked").val();
        if (tipo == 'fisica') {
            $('#documento').mask('000.000.000-00');
        } else {
            $('#documento').mask('99.999.999/9999-99');
        }
    });
});



function cadastro() {

    $('#log').css({display: "none"});
    $('#cad').css({display: "block"});
    $('#titulo').html('Cadastrar');
}
function login() {

    $('#log').css({display: "block"});
    $('#cad').css({display: "none"});
    $('#titulo').html('Login');
}

function previewImagem() {
    var imagem = document.querySelector('input[name=imagem]').files[0];
    var preview = document.getElementById('preview');

    var reader = new FileReader();

    reader.onloadend = function () {
        preview.src = reader.result;
    }

    if (imagem) {
        reader.readAsDataURL(imagem);
    } else {
        preview.src = "";
    }
}

function abrirtelaCadastro() {

    $.ajax({

        type: 'post',
        url: '/admin/editar-pecas',
        data: {'acao': 'cadastrar'},

        success: function (re) {
            $('#listar-produtos').css({display: "none"});
            $('#form').html(re);
            $('#cadastrar').css({display: "block"});
        },
        error: function () {
            $("#retorno").html("Erro em chamar a função.");
            setTimeout(function () {
                $("#retorno").css({display: "none"});
            }, 5000);
        }
    });
}

function abrirTelaEditar(id) {



    $.ajax({
        type: 'post',
        url: '/admin/editar-pecas',
        data: {'acao': 'editar', 'id': id},

        success: function (re) {
            $('#listar-produtos').css({display: "none"});
            $('#form').html(re);
            $('#cadastrar').css({display: "block"});
        },
        error: function () {
            $("#retorno").html("Erro em chamar a função.");
            setTimeout(function () {
                $("#retorno").css({display: "none"});
            }, 5000);
        }
    });
}

(function ($) {
    remove = function (item, id, img) {
        if (confirm("Deseja realmente excluir?")) {

            $.ajax({

                type: 'post',
                url: '/admin/excluir-peca',
                data: {'id': id, 'img': img},

                success: function (re) {

                    var tr = $(item).closest('tr');
                    tr.fadeOut(400, function () {
                        tr.remove();
                    });
                    return false;
                },
                error: function () {
                    $("#retorno").html("Erro em chamar a função.");
                    setTimeout(function () {
                        $("#retorno").css({display: "none"});
                    }, 5000);
                }
            });

        }

    }
})(jQuery);

$(function () {
    $("#tabela-produtos input").keyup(function () {
        var index = $(this).parent().index();
        var nth = "#tabela-produtos td:nth-child(" + (index + 1).toString() + ")";
        var valor = $(this).val().toUpperCase();
        $("#tabela-produtos tbody tr").show();
        $(nth).each(function () {
            if ($(this).text().toUpperCase().indexOf(valor) < 0) {
                $(this).parent().hide();
            }
        });
    });

    $("#tabela-produtos input").blur(function () {
        $(this).val("");
    });
});

function adicionarAoCarrinho(qtd, id) {
    $.ajax({

        type: 'post',
        url: '/carrinho/adicionar',
        data: {'id': id, 'qty': qtd},

        success: function (re) {
            $("#adicionados").html(" " + qtd);
            $("#retorno").html(re);
            return;
        },
        error: function () {
            $("#retorno").html("Erro em chamar a função.");
            setTimeout(function () {
                $("#retorno").css({display: "none"});
            }, 5000);
        }
    });

}

function adicionarMaquina() {
    var modelo = document.getElementById('modelo').value;
    $.ajax({

        type: 'post',
        url: '/adicionarMaquina',
        data: {'modelo': modelo},

        success: function (re) {
            alert(re);
            var apli = $('#categorias').html();
            $('#categorias').html(apli + "<div class='form-check'><input class='form-check-input' type='checkbox' name='aplicacao[]' value='" + re + "'>" +
                    "<label class='form-check-label'>" + modelo + "</label> </div>");
            return;
        },
        error: function () {
            $("#retorno").html("Erro em chamar a função.");
            setTimeout(function () {
                $("#retorno").css({display: "none"});
            }, 5000);
        }
    });

}

function pesquisar() {
    var codigo = document.getElementById('pesquisar').value;
    $.ajax({

        type: 'post',
        url: '/pesquisar-codigo',
        data: {'codigo': codigo},

        success: function (re) {
             $("#retorno").html(re);
        },
        error: function () {
            $("#retorno").html("Erro em chamar a função.");
            setTimeout(function () {
                $("#retorno").css({display: "none"});
            }, 5000);
        }
    });

}












