<?php

namespace IPECAS;

require_once __DIR__ . '/../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Exception\NoConfigurationException;
use Twig\Loader\FilesystemLoader;
use Twig\Environment;
use IPECAS\Util\Section;



$section = new Section();


$loader = new FilesystemLoader('../src/View/');
$twig = new Environment($loader);
//echo $twig->render('Produtos.html');


include 'rotas.php';
$response = new Response();

$contexto = new RequestContext();
$contexto->fromRequest(Request::createFromGlobals());
$matcher = new UrlMatcher($rotas, $contexto);

try {
    $configRota = $matcher->match($contexto->getPathInfo());
    $controlador = $configRota['controlador'];
    $objeto = new $controlador($response,$twig,$section);
    $metodo = $configRota['metodo'];
    if (isset($configRota['parametro'])) {
        $objeto->$metodo($configRota['parametro']);
    } else {
        $objeto->$metodo();
    }
} catch (ResourceNotFoundException $ex) {
   // $objeto = new ControladorProdutos($response, $twig, $section);
   // $objeto->padrao();
}

$response->send();

