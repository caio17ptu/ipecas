<?php

namespace IPECAS\Controladores;

    use Symfony\Component\HttpFoundation\Response;
    use Twig\Environment;
    use IPECAS\Util\Section;
    use IPECAS\Modelos\PecaModel;
    use IPECAS\Entidades\Peca;

class GerenciadorPecas {


    private $response;
    private $twig;
    private $sec;
    private $pModel;

    function __construct(Response $response, Environment $twig, Section $sec) {
        $this->response = $response;
        $this->twig = $twig;
        $this->sec = $sec;
        $this->pModel = new PecaModel();
    }
    
    
    public function exibirPeca($param) {
        $categorias = $this->pModel->listarMaquinas();
      
        $peca = $this->pModel->buscaPeca($param);
        $qtd=0;
        if (!empty($_SESSION['cart'])) {
             $qtd = $this->sec->verificarCarrinho($param);
        }
        if ($this->sec->get('user') != null) {
            return $this->response->setContent($this->twig->render('exibe-peca.twig', ['peca' => $peca, 'nome' => $this->sec->get('user'), 'qtd' => $qtd, 'aplicacoes' => $categorias]));
        }
        return $this->response->setContent($this->twig->render('exibe-peca.twig', ['peca' => $peca, 'nome' => false, 'qtd' => $qtd, 'aplicacoes' => $categorias,
                ]));

        
    }
    
    public function listarCategorias($param) {
        $pecas = $this->pModel->listaPorCategoria($param);
        $categorias = $this->pModel->listarMaquinas();
        $maq = $this->pModel->buscarMaquina($param);
         if ($this->sec->get('user') != null) {
             $user = $this->sec->get('user');
         }else{
             $user = false;
         }
        return $this->response->setContent($this->twig->render('listarProdutos.twig', ['aplicacoes' => $categorias, 'pecas' => $pecas, 'aplicacoes' => $categorias,
            'maquina' => $maq, 'user' => $user]));
        
        
    }
    public function pesquisar() {
        $codigo = $_POST['codigo'];
        
        $peca = $this->pModel->pesquisarPeca($codigo);
        if(!empty($peca)){
            echo '<script> window.location = "/pecas/detalhes/'.$peca['id'].'" </script>';
        }
        else{
            echo '';
        }
        
    }

}
