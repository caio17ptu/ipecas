<?php

namespace IPECAS\Controladores;

use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;
use IPECAS\Util\Section;
use IPECAS\Modelos\PecaModel;
use IPECAS\Entidades\Peca;

class GerenciadorAdmin {

    private $response;
    private $twig;
    private $sec;
    private $pModel;

    function __construct(Response $response, Environment $twig, Section $sec) {
        $this->response = $response;
        $this->twig = $twig;
        $this->sec = $sec;
        $this->pModel = new PecaModel();
    }

    public function gerenciarPecas() {
        $peca = $this->pModel->listarPecas();
        if ($this->sec->get('user') != null) {
            return $this->response->setContent($this->twig->render('administrarPecas.twig', ['user' => $this->sec->get('user'), 'pecas' => $peca]));
        }
        return $this->response->setContent($this->twig->render('administrarPecas.twig', ['user' => false, 'pecas' => $peca]));
    }

    public function listarCategorias() {
        $aux = $this->pModel->listarMaquinas();
        return $aux;
// return $this->response->setContent($this->twig->render('categorias.twig', ['maquinas' => $aux]));
    }

    public function cadastrarPeca() {

        $nome = $_POST['nome'];
        $preco = $_POST['preco'];
        $descricao = $_POST['descricao'];
        $codigo = $_POST['codigo'];


        $temp = $_FILES["imagem"]["tmp_name"];
        $mensagem = '';


        if (isset($_POST['aplicacao'])) {
            $categoria = $_POST['aplicacao'];
            if (empty($nome) || empty($preco) || empty($descricao) || empty($codigo) || empty($categoria) || empty($temp)) {
                $mensagem = 'Preencha todos os campos e selecione uma imagem';
            } else {

                $peca = new Peca($codigo, $preco, $descricao, $codigo, $nome);



                if (!$this->pModel->existeCodigo($codigo)) {
                    $id = $this->pModel->inserirPeca($peca, $categoria);
                    if ($id) {
                        if (move_uploaded_file($temp, '/var/www/html/IPECAS/public/imgPecas/'.$peca->getImagem())) {
                            $mensagem = 'Cadastro bem sucedido';
                        } else {
                            $mensagem = 'Erro ao mover arquivo';
                        }
                    } else {
                        $mensagem = 'Erro ao cadastrar';
                    }
                } else {
                    $mensagem = 'Não cadastrado, Código já possue cadastro';
                }
            }
        } else {
            $mensagem = 'Selecione ao menos uma Aplicação';
        }

        echo $mensagem;
    }

    public function editarPeca() {
        $nome = $_POST['nome'];
        $preco = $_POST['preco'];
        $descricao = $_POST['descricao'];
        $codigo = $_POST['codigo'];

        $id = $_POST['id'];
        $mensagem = '';
        $temp = $_FILES["imagem"]["tmp_name"];
        if (isset($_POST['aplicacao'])) {
            $categoria = $_POST['aplicacao'];
            if (empty($nome) || empty($preco) || empty($descricao) || empty($codigo) || empty($categoria)) {
                $mensagem = 'Preencha todos os campos';
            } else {
                if ($this->pModel->existeCodigo($codigo)) {
                    if ($this->pModel->existeCodigo($codigo)['id'] != $id) {
                        $mensagem = 'Código já cadastrado';
                    } else {
                        if (empty($temp)) {
                            $img = $this->pModel->buscaPeca($id)['imagem'];
                            $peca = new Peca($codigo, $preco, $descricao, $codigo, $nome);
                            if ($this->pModel->editarPeca($peca, $id, $categoria)) {
                                $mensagem = 'Alterado com sucesso';
                            } else {
                                $mensagem = 'Erro ao alterar';
                            }
                        } else {
                            $img = $_FILES['imagem']['name'];
                            $peca = new Peca($codigo, $preco, $descricao, $codigo, $nome);
                            if ($this->pModel->editarPeca($peca, $id, $categoria)) {
                                if (move_uploaded_file($temp, '/var/www/html/IPECAS/public/imgPecas/'.$peca->getImagem())) {
                                    $mensagem = 'Alterado com sucesso';
                                } else {
                                    $mensagem = 'Erro ao mover arquivo';
                                }
                            } else {
                                $mensagem = 'Erro ao alterar';
                            }
                        }
                    }
                }
            }
        } else {
            $mensagem = 'Selecione ao menos uma Aplicação';
        }



        echo $mensagem;
    }

    public function abrirEditorPecas() {

        if ($_POST['acao'] == 'cadastrar') {
            $maquinas = $this->listarCategorias();
            $acao = 'cadastrar';
            $peca = false;
            $aplicacoes = false;
        } else {
            $acao = 'editar';
            $id = $_POST['id'];
            $peca = $this->pModel->buscaPeca($id);
            $maquinas = $this->pModel->getAplicoesPeca($id);
        }

        return $this->response->setContent($this->twig->render('editar-peca.twig', ['user' => false, 'maquinas' => $maquinas, 'peca' => $peca, 'acao' => $acao]));
    }

    public function verificarCodigo() {
        $cod = $_POST['cod'];
        if ($this->pModel->existeCodigo($cod)) {
            echo 'Código já cadastrado';
        } else {
            echo '';
        }
    }
    public function excluirPeca() {
        $id = $_POST['id'];
        $img = $_POST['img'];
        if(unlink('/var/www/html/IPECAS/public/imgPecas/'.$img)){
            $this->pModel->excluirPeca($id);
        }
        
        return;
        
    }
    public function adicionarMaquina() {
        $modelo = $_POST['modelo'];
        echo $this->pModel->cadastrarMaquina($modelo);
        
    }

}
