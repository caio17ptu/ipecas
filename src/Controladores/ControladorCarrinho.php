<?php

namespace IPECAS\Controladores;

use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;
use IPECAS\Util\Section;
use IPECAS\Modelos\PecaModel;
use IPECAS\Modelos\LoginModel;

class ControladorCarrinho {
    
    
    private $response;
    private $twig;
    private $sec;
    private $pModel;

    
    
    public function __construct(Response $response, Environment $twig, Section $sec) {
        $this->response = $response;
        $this->twig = $twig;
        $this->sec = $sec;
        
    
        $this->pModel = new PecaModel();
    }
    public function listarPecas(){
        $pecas = false;
        $aplicaoes = $this->pModel->listarMaquinas();
        if (empty($_SESSION['cart'])) {
            $pecas = false;
         }else{
               $pecas = $this->getPecas();
               
         }
        if ($this->sec->get('user') != null) {
            return $this->response->setContent($this->twig->render('carrinho.twig', ['user' => $this->sec->get('user'), 'pecas' => $pecas, 'aplicacoes' => $aplicaoes]));
        }
        return $this->response->setContent($this->twig->render('carrinho.twig', ['user' => false, 'pecas' => $pecas, 'aplicacoes' => $aplicaoes]));
    }
    
     public function finalizarVenda() {
       $pecas = false;
        if (empty($_SESSION['cart'])) {
            $pecas = false;
         }else{
               $pecas = $this->getPecas();
         }
        if ($this->sec->get('user') != null) {
            $lModel = new LoginModel();
            $user = $this->sec->get('user');
            $aplicaoes = $this->pModel->listarMaquinas();
            $enderecos = $lModel->listarenderecosCliente($user[0]['id_cliente']);
            return $this->response->setContent($this->twig->render('finalizarCompra.twig',['user' => $this->sec->get('user'), 'pecas' => $pecas, 'enderecos' => $enderecos,
                'aplicacoes' => $aplicaoes]));
        }
        $this->sec->set('anterior', '/checkout');
        return $this->response->setContent($this->twig->render('login.twig', ['user' => false]));
    }
        
    
    public function adicionarPeca(){
        $idPeca = $_POST['id'];
        $qtd = $_POST['qty'];
        
        $array = ['id' => $idPeca, 'qtd' => $qtd];
        
        echo($this->sec->addPecaCart($array));
       
        
    }

    public function getPecas() {
        $pecas=[];
        
        foreach ($_SESSION['cart'] as $key => $value){
            $peca = $this->pModel->buscaPecaArray($value['id']);
            $peca['qtd'] = intval($value['qtd']);
            array_push($pecas, $peca);
       }
       return $pecas;
    }
    public function salvarPedido() {
        if (empty($_SESSION['cart'])) {
            echo '<script> window.location = "/carrinho/listar-pecas" </script>';
            return;
        }
        $pecas = $this->getPecas();
        $total = 0;
        foreach ($pecas as $c) {
            $total+=$c['qtd'] * $c['preco'];
        }
        $user = $this->sec->get('user')[0]['id_cliente'];
        $this->pModel->adcionarPedido($pecas, $user, $total);
        unset($_SESSION['cart']);
        
        
        echo '<script> window.location = "/Pedidos" </script>';
        
        
        
    }
    public function listarPedidos() {
        if ($this->sec->get('user') != null) {
            $pedidos = $this->pModel->listarPedidos($this->sec->get('user')[0]['id_cliente']);
            
        }
        return $this->response->setContent($this->twig->render('exibePedidos.twig', ['user' => $this->sec->get('user'), 'pedidos' => $pedidos]));
    }
}
