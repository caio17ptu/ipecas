<?php

namespace IPECAS\Controladores;

use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;
use IPECAS\Util\Section;
use IPECAS\Entidades\Login;
use IPECAS\Modelos\LoginModel;
use IPECAS\Modelos\PecaModel;
use IPECAS\Entidades\Endereco;

class ControladorIndex {

    private $response;
    private $twig;
    private $sec;

    function __construct(Response $response, Environment $twig, Section $sec) {
        $this->response = $response;
        $this->twig = $twig;
        $this->sec = $sec;
    }

    public function indexPadrao() {
        $pModel = new PecaModel();
        $categorias = $pModel->listarMaquinas();
        $peca = $this->listarpecas();
        if ($this->sec->get('user') != null) {
            return $this->response->setContent($this->twig->render('index.twig', ['pecas' => $peca, 'user' => $this->sec->get('user'), 'aplicacoes' => $categorias]));
        }
        return $this->response->setContent($this->twig->render('index.twig', ['pecas' => $peca, 'user' => false, 'aplicacoes' => $categorias]));
    }

    public function logar() {
        $this->sec->set('anterior', '/');
        return $this->response->setContent($this->twig->render('login.twig'));
    }

    public function cadastrar() {
        return $this->response->setContent($this->twig->render('login.twig'));
    }

    public function inserirLogin() {

        $tipo = $_POST['tipo'];
        $nome = $_POST['nome'];
        $sobrenome = $_POST['sobrenome'];
        $fone = $_POST['fone'];
        $documento = $_POST['documento'];
        $email = $_POST['email'];
        $rua = $_POST['rua'];
        $numero = $_POST['numero'];
        $bairro = $_POST['bairro'];
        $cidade = $_POST['cidade'];
        $uf = $_POST['estado'];
        $pais = $_POST['pais'];
        $cep = $_POST['cep'];
        $senha1 = $_POST['senha'];
        $senha2 = $_POST['verifica-senha'];

        if ($senha1 != $senha2) {
            $erro = 'Senhas diferentes';
        } else if (empty($nome) || empty($tipo) || empty($sobrenome) || empty($fone) || empty($documento) || empty($email) || empty($rua) ||
                empty($bairro) || empty($cidade) || empty($uf) || empty($pais) || empty($cep) || empty ($numero)) {
            $erro = 'Preencha todos os campos!!';
        } else {
            $endereco = new Endereco($rua, $bairro, $cidade, $pais, $uf, $cep, $numero);
            $login = new Login($nome, $email, $senha1, $fone, $sobrenome, $documento, $tipo);

            $modelLogin = new LoginModel();
            $erro = $modelLogin->Cadastrar($login, $endereco);
            if ($erro) {
                $this->sec->set('nome', $nome);
                echo '<script> window.location = "/" </script>';
            } else {
                return 'falha ao cadastrar';
            }
        }
    }

    public function validarLogin() {
        $nome = $_POST['email'];
        $senha = $_POST['senha'];
        $modelLogin = new LoginModel();
        $login = $modelLogin->validarLogin($nome, $senha);

        if ($login) {
            $this->sec->setUser($login);
            echo '<script> window.location = "'.$this->sec->get('anterior').'" </script>';
        } else {
            echo 'nao logado';
        }
    }

    public function listarpecas() {
        $model = new PecaModel();
        $peca = $model->listarPecas();
        return $peca;
    }

    public function sair() {
        $this->sec->remove('user');
        echo '<script> window.location = "/" </script>';
    }
    

}
