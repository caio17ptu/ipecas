<?php


namespace IPECAS\Util;

class Section {
    
    function __construct() {
        session_start();
        
    }
    
    public function set($chave, $valor){
        $_SESSION[$chave] = $valor;
    }
    
    public function get($chave) {
        if (isset($_SESSION[$chave])) {
            return $_SESSION[$chave];
        } else {
            return null;
        }
    }
    
    public function remove($chave) {
        $_SESSION[$chave] = null;
    }
    
    public function addPecaCart($array) {

        if (empty($_SESSION['cart'])) {
            $_SESSION['cart'] = [];
         }
         $i=0;
         foreach ($_SESSION['cart'] as $key => $value){
            if($value['id'] == $array['id']){
                if(intval($array['qtd'] == 0)){
                    unset($_SESSION['cart'][$i]);
                    return 'removido do carrinho';
                }
               $_SESSION['cart'][$i]['qtd'] = intval($array['qtd']);
               return 'Quantidade alterada';
           }
           $i++;
       }
       if(intval($array['qtd'])>0){
       array_push($_SESSION['cart'], $array);
       return 'Item adicionado';
       }
         
        
    }
    public function setUser($param) {
        if (empty($_SESSION['user'])) {
            $_SESSION['user'] = [];
         }
         array_push($_SESSION['user'], $param);
    }
    public function verificarCarrinho($id) {

         foreach ($_SESSION['cart'] as $key => $value){
            if($value['id'] == $id){
                
                   return (intval($value['qtd']));   
           }
         
       }
        return 0;
    }

    
    
}
