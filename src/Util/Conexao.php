<?php

namespace IPECAS\Util;

use PDO;

class Conexao {

    private static $instancia;

    private function __construct() {
        
    }

    public static function getInstance() {
        if (self::$instancia) {
            return self::$instancia;
        }
        $dsn = 'mysql:host=localhost;dbname=ipecas';
        $username = 'root';
        $password = '341766';
        $options = [
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_ORACLE_NULLS => PDO::NULL_EMPTY_STRING
        ];

        self::$instancia = new PDO($dsn, $username, $password, $options);
        return self::$instancia;
    }

}
