<?php

namespace IPECAS\Modelos;

use IPECAS\Entidades\Login;
use IPECAS\Entidades\Endereco;
use IPECAS\Util\Conexao;
use PDO;

class LoginModel {

    public function __construct() {
        
    }

    public function Cadastrar(Login $login, Endereco $endereco) {
        $idCli = $this->buscarProximoIdCliente();
        try {
            $sql = 'insert into cliente (id_cliente, nome, sobrenome, email, senha, telefone, documento, tipo) values ( :id, :nome, :sobrenome, :email, :senha, :telefone, :documento, :tipo);';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $idCli);
            $p_sql->bindValue(':nome', $login->getNome());
            $p_sql->bindValue(':sobrenome', $login->getSobrenome());
            $p_sql->bindValue(':email', $login->getEmail());
            $p_sql->bindValue(':senha', $login->getSenha());
            $p_sql->bindValue(':telefone', $login->getTelefone());
            $p_sql->bindValue(':documento', $login->getCpf());
            $p_sql->bindValue(':tipo', $login->getTipo());
            if ($p_sql->execute()) {
                if ($this->cadastrarEnderec($endereco, $idCli,"Pricipal")) {
                    return $idCli;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception $exc) {
            return false;
        }
    }

    public function cadastrarEnderec(Endereco $endereco, $id, $nome) {
        $idEnd = $this->buscarProximoIdEndereco();
        try {
            $sql = 'insert into endereco (id_endereco, rua, numero, bairro, cidade, pais, estado, cep, id_cliente, nome) values (:id, :rua, :numero, :bairro, :cidade, :pais, :estado, :cep, :id_cliente, :nome);';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $idEnd);
            $p_sql->bindValue(':rua', $endereco->getRua());
            $p_sql->bindValue(':numero', $endereco->getNumero());
            $p_sql->bindValue(':bairro', $endereco->getBairro());
            $p_sql->bindValue(':cidade', $endereco->getCidade());
            $p_sql->bindValue(':pais', $endereco->getPais());
            $p_sql->bindValue(':estado', $endereco->getEstado());
            $p_sql->bindValue(':cep', $endereco->getCep());
            $p_sql->bindValue(':id_cliente', $id);
            $p_sql->bindValue(':nome', $nome);
            if ($p_sql->execute()) {
                return true;
            }
        } catch (Exception $exc) {
            return false;
        }
    }

    public function validarLogin($email, $senha) {
        try {
            $sql = 'SELECT * FROM cliente where email = :email and senha = :senha';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':senha', $senha);
            $p_sql->bindValue(':email', $email);
            if ($p_sql->execute()) {
                $user = $p_sql->fetch(PDO::FETCH_ASSOC);

                if (!empty($user)) {
                    return $user;
                }
            }
            return false;
        } catch (Exception $exc) {
            print 'Erro no banco de dados: ' . $exc;
        }
    }

    private function buscarProximoIdCliente() {
        try {
            $sql = 'SELECT max(id_cliente) +1 as id from cliente;';
            $p_sql = Conexao::getInstance()->prepare($sql);
            if ($p_sql->execute()) {
                $id = $p_sql->fetch(PDO::FETCH_ASSOC);

               if (!empty($id['id'])) {
                    return $id['id'];
                }
                return 1;
            }
        } catch (Exception $ex) {
            
        }
    }

    private function buscarProximoIdEndereco() {
        try {
            $sql = 'SELECT max(id_endereco)+1 as id from endereco;';
            $p_sql = Conexao::getInstance()->prepare($sql);
            if ($p_sql->execute()) {
                $id = $p_sql->fetch(PDO::FETCH_ASSOC);
                 if (!empty($id['id'])) {
                    return $id['id'];
                }
                return 1;
            }
        } catch (Exception $ex) {
            
        }
    }
    public function listarenderecosCliente($idCliente) {
        try {
            $sql = 'SELECT * FROM ipecas.endereco where endereco.id_cliente = :id;';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $idCliente);
            if ($p_sql->execute()) {
                $endereco = $p_sql->fetchAll(PDO::FETCH_ASSOC);
                return $endereco;
            }
            
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        
        }

}
