<?php

namespace IPECAS\Modelos;

use IPECAS\Util\Conexao;
use IPECAS\Entidades\Peca;
use PDO;

class PecaModel {

    public function inserirPeca(Peca $peca, $categorias) {
        $id = $this->buscarProximoIdPeca();
        try {
            $sql = 'insert into peca(id, codigo, descricao, preco, imagem, nome) values (:id ,:cod, :descricao, :valor, :imagem, :nome);';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue('id', $id);
            $p_sql->bindValue(':cod', $peca->getCodFabricante());
            $p_sql->bindValue(':descricao', $peca->getDescricao());
            $p_sql->bindValue(':valor', $peca->getValor());
            $p_sql->bindValue(':nome', $peca->getNome());
            $p_sql->bindValue(':imagem', $peca->getImagem());
            if ($p_sql->execute()) {

                $this->cadastrarAplicacoes($id, $categorias);
                return $id;
            } else {
                return false;
            }
        } catch (Exception $ex) {
            return false;
        }
    }

    public function editarPeca(Peca $peca, $id, $categorias) {
        try {
            $sql = "UPDATE peca SET nome = :nome, codigo= :cod, descricao= :descricao, preco= :valor, imagem= :imagem WHERE id=:id;";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue('id', $id);
            $p_sql->bindValue(':cod', $peca->getCodFabricante());
            $p_sql->bindValue(':descricao', $peca->getDescricao());
            $p_sql->bindValue(':valor', $peca->getValor());
            $p_sql->bindValue(':nome', $peca->getNome());
            $p_sql->bindValue(':imagem', $peca->getImagem());
            if ($p_sql->execute()) {
                if ($this->editarAplicaoes($id, $categorias)) {
                    return true;
                }
                return false;
            } else {
                return false;
            }
        } catch (Exception $ex) {
            return false;
        }
    }

    public function existeCodigo($cod) {
        try {
            $sql = 'SELECT * FROM ipecas.peca where codigo = :cod;';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':cod', $cod);
            $p_sql->execute();
            $codigo = $p_sql->fetch(PDO::FETCH_ASSOC);
            if (empty($codigo)) {
                return false;
            } else {
                return $codigo;
            }
        } catch (Exception $ex) {
            return 'Erro mysql:' . $ex;
        }
    }

    public function cadastrarAplicacoes($id, $categorias) {
        foreach ($categorias as $c) {
            try {
                $sql = 'insert into aplicacao (id_maq, id_peca) values (:idm, :id)';
                $p_sql = Conexao::getInstance()->prepare($sql);
                $p_sql->bindValue(':idm', $c);
                $p_sql->bindValue(':id', $id);
                $p_sql->execute();
            } catch (Exception $ex) {
                print 'Erro no banco de dados: ' . $ex;
            }
        }
    }

    public function editarAplicaoes($id, $categorias) {
        try {
            $sql = 'DELETE FROM aplicacao WHERE id_peca= :id';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue('id', $id);
            if ($p_sql->execute()) {
                $this->cadastrarAplicacoes($id, $categorias);
                return true;
            } else {
                return false;
            }
        } catch (Exception $ex) {
            return false;
        }
    }

    public function listarMaquinas() {
        try {
            $sql = 'SELECT * FROM ipecas.maquina;';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_ASSOC);
        } catch (Exception $ex) {
            return 'Erro mysql:' . $ex;
        }
    }

    public function buscarMaquina($id) {
        try {
            $sql = 'SELECT * FROM ipecas.maquina where id_maquina = :id;';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $id);
            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_ASSOC);
        } catch (Exception $ex) {
            return 'Erro mysql:' . $ex;
        }
    }

    public function listarPecas() {
        try {
            $sql = 'SELECT * FROM ipecas.peca;';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            return 'Erro mysql:' . $ex;
        }
    }

    public function buscaPeca($id) {
        try {
            $sql = 'SELECT * FROM ipecas.peca where id = :id;';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $id);
            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_ASSOC);
        } catch (Exception $ex) {
            return 'Erro mysql:' . $ex;
        }
    }

    public function buscaPecaArray($id) {
        try {
            $sql = 'SELECT * FROM ipecas.peca where id = :id;';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $id);
            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_ASSOC);
        } catch (Exception $ex) {
            return 'Erro mysql:' . $ex;
        }
    }

    private function buscarProximoIdPeca() {
        try {
            $sql = 'SELECT max(id)+1 as id from peca;';
            $p_sql = Conexao::getInstance()->prepare($sql);
            if ($p_sql->execute()) {
                $id = $p_sql->fetch(PDO::FETCH_ASSOC);
                if (!empty($id['id'])) {
                    return $id['id'];
                }
                return 1;
            }
        } catch (Exception $ex) {
            
        }
    }

    public function getAplicoesPeca($idPeca) {
        $sql = 'SELECT m.modelo, m.id_maquina from aplicacao as a join peca as p on p.id = a.id_peca join maquina as m on m.id_maquina = a.id_maq where p.id = :id;';
        try {
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $idPeca);
            $p_sql->execute();
            $aplicaces = $p_sql->fetchAll(PDO::FETCH_ASSOC);
        } catch (Exception $ex) {
            echo" erro " + $ex;
        }
        $maquinas = $this->listarMaquinas();
        $i = 0;
        foreach ($maquinas as $key => $value) {

            foreach ($aplicaces as $key => $value2) {

                if ($value['id_maquina'] == $value2['id_maquina']) {
                    $maquinas[$i]['status'] = "true";
                    break;
                }
                $maquinas[$i]['status'] = "false";
            }

            $i++;
        }
        return $maquinas;
    }

    public function excluirPeca($id) {
        $sql = 'delete from peca where id = :id;';
        try {
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $id);
            $p_sql->execute();
            try {
                $sql = 'delete from aplicacao where id_peca = :id;';
                $p_sql->bindValue(':id', $id);
                $p_sql->execute();
            } catch (Exception $ex) {
                
            }
            return;
        } catch (Exception $ex) {
            
        }
    }

    public function listaPorCategoria($id) {
        $sql = 'select p.id, p.codigo, p.descricao, p.imagem, p.nome, p.preco from peca as p join aplicacao as a on p.id = a.id_peca where a.id_maq = :id;';
        try {
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $id);
            $p_sql->execute();

            $pecas = $p_sql->fetchAll(PDO::FETCH_ASSOC);
            return $pecas;
        } catch (Exception $ex) {
            
        }
    }

    public function cadastrarMaquina($modelo) {
        $id = $this->buscarProximoIdCategoria();
        $sql = "insert into maquina (id_maquina, modelo) values (:id ,:modelo);";
        try {
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $id);
            $p_sql->bindValue(':modelo', $modelo);
            $p_sql->execute();
            return $id;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    private function buscarProximoIdCategoria() {
        try {
            $sql = 'SELECT max(id_maquina)+1 as id from maquina;';
            $p_sql = Conexao::getInstance()->prepare($sql);
            if ($p_sql->execute()) {
                $id = $p_sql->fetch(PDO::FETCH_ASSOC);
                if (!empty($id['id'])) {
                    return $id['id'];
                }
                return 1;
            }
        } catch (Exception $ex) {
            
        }
    }

    public function adicionarPecasPedido($idPedido, $pecas) {
        foreach ($pecas as $c) {
            try {
                $sql = 'insert into pecasPedido (id_pedido, codigo_peca, nome_peca, descricao_peca, imagem_peca, preco_peca) values (:idP, :codigo, :nome, :descricao, :imagem, :preco)';
                $p_sql = Conexao::getInstance()->prepare($sql);
                $p_sql->bindValue(':idP', $idPedido);
                $p_sql->bindValue(':codigo', $c['codigo']);
                $p_sql->bindValue(':nome', $c['nome']);
                $p_sql->bindValue(':descricao', $c['descricao']);
                $p_sql->bindValue(':imagem', $c['imagem']);
                $p_sql->bindValue(':preco', $c['preco']);
                $p_sql->execute();
            } catch (Exception $ex) {
                return false;
            }
        }
        return true;
    }

    public function adcionarPedido($pecas, $user, $total) {
        $idPedido = $this->buscarProximoIdPedido();
        try {
            $sql = 'insert into pedido (id_cliente , id_pedido, total, data) values (:idCliente, :idPedido, :total, :data);';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':idCliente', $user);
            $p_sql->bindValue(':idPedido', $idPedido);
            $p_sql->bindValue(':total', $total);
            $p_sql->bindValue(':data', date('d/m/Y'));
            if ($p_sql->execute()) {
                if ($this->adicionarPecasPedido($idPedido, $pecas)) {
                    return true;
                }
            }
            return false;
        } catch (Exception $ex) {
            return false;
        }
    }

    private function buscarProximoIdPedido() {
        try {
            $sql = 'SELECT max(id_pedido)+1 as id from pedido;';
            $p_sql = Conexao::getInstance()->prepare($sql);
            if ($p_sql->execute()) {
                $id = $p_sql->fetch(PDO::FETCH_ASSOC);
                if (!empty($id['id'])) {
                    return $id['id'];
                }
                return 1;
            }
        } catch (Exception $ex) {
            
        }
    }

    public function listarPecasPedidos($param) {
        $sql = 'SELECT pd.codigo_peca, pd.nome_peca, pd.preco_peca FROM pedido as p join pecasPedido as pd on pd.id_pedido = p.id_pedido where p.id_pedido = :id;';
        try {
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $param);
            if ($p_sql->execute()) {
                $pecas = $p_sql->fetchAll(PDO::FETCH_ASSOC);
                return $pecas;
            }
        } catch (Exception $ex) {
            
        }
    }

    public function listarPedidos($idCliente) {
        try {
            $sql = 'SELECT * from pedido where id_cliente = :idCliente;';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':idCliente', $idCliente);
            if ($p_sql->execute()) {
                $pedidos = $p_sql->fetchAll(PDO::FETCH_ASSOC);
                $i =0;
                foreach ($pedidos as $p) {
                    $pedidos[$i]['pecas'] = $this->listarPecasPedidos($pedidos[$i]['id_pedido']);
                    $i++;
                }
                return $pedidos;
            }
        } catch (Exception $ex) {
            
        }
    }
    
    public function pesquisarPeca($codigo) {
        try {
            $sql = 'SELECT * FROM ipecas.peca where codigo = :id;';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $codigo);
            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_ASSOC);
        } catch (Exception $ex) {
            return 'Erro mysql:' . $ex;
        }
    }

}
