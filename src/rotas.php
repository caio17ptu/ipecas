<?php

namespace IPECAS;

use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;

$rotas = new RouteCollection();

$rotas->add('/',new Route('/',['controlador'=>'\IPECAS\Controladores\ControladorIndex',
    'metodo' => 'indexPadrao'
    
    ]
        ));

$rotas->add('Administrar',new Route('/admin',['controlador'=>'\IPECAS\Controladores\GerenciadorAdmin',
    'metodo' => 'gerenciar'
    
    ]
        ));

$rotas->add('cadastrarPecas',new Route('/admin/cadastrar-pecas',['controlador'=>'\IPECAS\Controladores\GerenciadorAdmin',
    'metodo' => 'cadastrarPeca'
    
    ]
        ));
$rotas->add('logar',new Route('/login',['controlador'=>'\IPECAS\Controladores\ControladorIndex',
    'metodo' => 'logar'
    
    ]
        ));
$rotas->add('validarLogin',new Route('/login/validar',['controlador'=>'\IPECAS\Controladores\ControladorIndex',
    'metodo' => 'validarLogin'
    
    ]
        ));

$rotas->add('cadastrarLogin',new Route('/login/cadastrar',['controlador'=>'\IPECAS\Controladores\ControladorIndex',
    'metodo' => 'inserirLogin'
    
    ]
        ));
$rotas->add('sair',new Route('/login/sair',['controlador'=>'\IPECAS\Controladores\ControladorIndex',
    'metodo' => 'sair'
    
    ]
        ));
$rotas->add('listarCategorias',new Route('/admin/listar-categorias',['controlador'=>'\IPECAS\Controladores\GerenciadorAdmin',
    'metodo' => 'listarCategorias'
    
    ]
        ));
$rotas->add('detalharPeca',new Route('/pecas/detalhes/{parametro}',['controlador'=>'\IPECAS\Controladores\GerenciadorPecas',
    'metodo' => 'exibirPeca'], ['parametro'=>'.*']
        ));
       
$rotas->add('listar-categorias',new Route('/pecas/listar-categorias/{parametro}',['controlador'=>'\IPECAS\Controladores\GerenciadorPecas',
    'metodo' => 'listarCategorias'], ['parametro'=>'.*']
        ));

$rotas->add('AdicionarNoCarrinho',new Route('/carrinho/adicionar',['controlador'=>'\IPECAS\Controladores\ControladorCarrinho',
    'metodo' => 'adicionarPeca']
        ));
$rotas->add('ListarCarrinho',new Route('/carrinho/listar-pecas',['controlador'=>'\IPECAS\Controladores\ControladorCarrinho',
    'metodo' => 'listarPecas']
        ));
$rotas->add('finalizarVenda',new Route('/checkout',['controlador'=>'\IPECAS\Controladores\ControladorCarrinho',
    'metodo' => 'finalizarVenda']
        ));
$rotas->add('AdministrarPecas',new Route('/admin/pecas',['controlador'=>'\IPECAS\Controladores\GerenciadorAdmin',
    'metodo' => 'gerenciarPecas']
        ));

$rotas->add('EditarPecas',new Route('/admin/editar-pecas',['controlador'=>'\IPECAS\Controladores\GerenciadorAdmin',
    'metodo' => 'abrirEditorPecas']
        ));

$rotas->add('AlterarPeca',new Route('/admin/alterar-peca',['controlador'=>'\IPECAS\Controladores\GerenciadorAdmin',
    'metodo' => 'editarPeca']
        ));
$rotas->add('verificarCodigo',new Route('/verificarCodigo',['controlador'=>'\IPECAS\Controladores\GerenciadorAdmin',
    'metodo' => 'verificarCodigo']
        ));
$rotas->add('excluir-peca',new Route('/admin/excluir-peca',['controlador'=>'\IPECAS\Controladores\GerenciadorAdmin',
    'metodo' => 'excluirPeca']
        ));
$rotas->add('AddMaquina',new Route('/adicionarMaquina',['controlador'=>'\IPECAS\Controladores\GerenciadorAdmin',
    'metodo' => 'adicionarMaquina']
        ));

$rotas->add('AdicionarPedido',new Route('/adicionarPedido',['controlador'=>'\IPECAS\Controladores\ControladorCarrinho',
    'metodo' => 'salvarPedido']
        ));
$rotas->add('listarPedido',new Route('/Pedidos',['controlador'=>'\IPECAS\Controladores\ControladorCarrinho',
    'metodo' => 'listarPedidos']
        ));
$rotas->add('pesquisar',new Route('/pesquisar-codigo',['controlador'=>'\IPECAS\Controladores\GerenciadorPecas',
    'metodo' => 'pesquisar']
        ));

return $rotas;