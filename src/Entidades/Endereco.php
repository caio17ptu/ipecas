<?php


namespace IPECAS\Entidades;

class Endereco {
   private $rua;
   private $bairro;
   private $cidade;
   private $pais;
   private $estado;
   private $cep;
   private $numero;
   
   function __construct($rua, $bairro, $cidade, $pais, $estado, $cep, $numero) {
       $this->rua = $rua;
       $this->bairro = $bairro;
       $this->cidade = $cidade;
       $this->pais = $pais;
       $this->estado = $estado;
       $this->cep = $cep;
       $this->numero = $numero;
   }
   
   function getRua() {
       return $this->rua;
   }

   function getBairro() {
       return $this->bairro;
   }

   function getCidade() {
       return $this->cidade;
   }

   function getPais() {
       return $this->pais;
   }

   function getEstado() {
       return $this->estado;
   }

   function getCep() {
       return $this->cep;
   }

   function setRua($rua) {
       $this->rua = $rua;
   }

   function setBairro($bairro) {
       $this->bairro = $bairro;
   }

   function setCidade($cidade) {
       $this->cidade = $cidade;
   }

   function setPais($pais) {
       $this->pais = $pais;
   }

   function setEstado($estado) {
       $this->estado = $estado;
   }

   function setCep($cep) {
       $this->cep = $cep;
   }
   function getNumero() {
       return $this->numero;
   }

   function setNumero($numero) {
       $this->numero = $numero;
   }




           
}
