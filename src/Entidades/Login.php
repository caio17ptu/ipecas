<?php

namespace IPECAS\Entidades;


class Login {
   private $nome;
   private $email;
   private $senha;
   private $imagem;
   private $cpf;
   private $sobrenome;
   private $telefone;
   private $tipo;
   
   public function __construct($nome, $email, $senha, $telefone, $sobrenome,$cpf ,$tipo) {
       $this->email=$email;
       $this->nome=$nome;
       $this->senha=$senha;
       $this->cpf=$cpf;
       $this->sobrenome=$sobrenome;
       $this->telefone=$telefone;
       $this->tipo=$tipo;
   }
   
   function getNome() {
       return $this->nome;
   }

   function getEmail() {
       return $this->email;
   }

   function getSenha() {
       return $this->senha;
   }

   function getImagem() {
       return $this->imagem;
   }

   function setNome($nome) {
       $this->nome = $nome;
   }

   function setEmail($email) {
       $this->email = $email;
   }

   function setSenha($senha) {
       $this->senha = $senha;
   }

   function setImagem($imagem) {
       $this->imagem = $imagem;
   }
   function getCpf() {
       return $this->cpf;
   }

   function getSobrenome() {
       return $this->sobrenome;
   }

   function getTelefone() {
       return $this->telefone;
   }

   function setCpf($cpf) {
       $this->cpf = $cpf;
   }

   function setSobrenome($sobrenome) {
       $this->sobrenome = $sobrenome;
   }

   function setTelefone($telefone) {
       $this->telefone = $telefone;
   }
   function getTipo() {
       return $this->tipo;
   }

   function setTipo($tipo) {
       $this->tipo = $tipo;
   }


    
}
