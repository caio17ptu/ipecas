<?php

namespace IPECAS\Entidades;

class Peca {
    private $id;
    private $CodFabricante;
    private $Valor;
    private $descricao;
    private $imagem;
    private $nome;
    
    public function __construct($codFabricante, $valor, $descricao, $imagem, $nome) {
        $this->CodFabricante=$codFabricante;
        $this->Valor=$valor;
        $this->descricao=$descricao;
        $this->imagem=$imagem;
        $this->nome=$nome;
        
        
    }
    public function _construct(){
        
    }
            
    function getId() {
        return $this->id;
    }

    function getCodFabricante() {
        return $this->CodFabricante;
    }

    function getValor() {
        return $this->Valor;
    }

    function getDescricao() {
        return $this->descricao;
    }

    function getImagem() {
        return $this->imagem;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setCodFabricante($CodFabricante) {
        $this->CodFabricante = $CodFabricante;
    }

    function setValor($Valor) {
        $this->Valor = $Valor;
    }

    function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

    function setImagem($imagem) {
        $this->imagem = $imagem;
    }
    function getNome() {
        return $this->nome;
    }



    
   
    
    
}
